/*hm1)Дано число (рандом1-100)
При попадании его в промежуток (0;20], вывести остаток от деления на 8;
При попадании [15;45) вывести остаток от деления на 10;
При попадании в (45,70] умножить на 5/8;
В другом случае розделить на 5 и вывести на экран.*/


import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        int i = random.nextInt(100)+1;
        System.out.println("Случайно сгенерированное число " + i);
        if (i > 1 && i <= 20) {
            System.out.println(i % 8);
        }
        if (i >= 15 && i < 45) {
            System.out.println(i % 10);
        }
        if(i>45 && i<=70) { System.out.print(i * (5 / 8));}
        if (i >= 71 && i < 100) {
            System.out.println(i / 5);
        }
        if (i == 45) {
            System.out.println(i / 5);
        }

    }
}
